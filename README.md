# (semver versions) and (semver ranges)

This repository contains R6RS Scheme libraries for working
with [Semantic Versioning][semver] (SemVer) and [ranges][ranges].

 [semver]: https://semver.org/
 [ranges]: https://docs.npmjs.com/misc/semver

## Versions API

The versions API implements Semantic Versioning 2.0.0.

```Scheme
(import (semver versions))
```

### (semver? obj)

Returns `#t` if *obj* is a semver, otherwise returns `#f`.

### (make-semver major minor patch [pre-release-ids] [build-ids])

Returns a newly allocated semver object with version
*major*.*minor*.*patch*.

Optionally lists of pre-release and build identifiers can be provided.

Each of *major*, *minor* and *patch* must be an exact non-negative
integer.

Pre-release identifiers may be either an exact non-negative integers
or strings. The strings may be fully numeric, but can in that case not
start with a zero. The allowed characters are 0-9, A-Z, a-z and hyphen
(#\-).

Build identifiers must be strings. The allowed characters are 0-9,
A-Z, a-z and hyphen (#\-).

If the arguments do not form a valid semver, **make-semver** raises an
exception with condition type **&assertion**.

### (semver-major semver)

Returns the major part of *semver*, n.0.0, as a number.

### (semver-minor semver)

Returns the minor part of *semver*, 0.n.0, as a number.

### (semver-patch semver)

Returns the patch part of *semver*, 0.0.n, as a number.

### (semver-pre-release-ids semver)

Returns the pre-release identifiers of the semver, 0.0.0-ids, as a
list. Each element is either a number or string. Fully numeric string
identifiers are represented as numbers.

### (semver-build-ids semver)

Returns the build identifiers of the semver, 0.0.0+ids, as a list.
Each element is a string.

### (string->semver string)

Returns a newly allocated semver object that represents the same
version as *string*.

If *string* does not represent a valid semver, **string->semver**
raises an exception with condition type **&assertion**.

### (semver->string semver)

Returns a newly allocated string that represents the same version as
*semver*.

### (semver-compare semver0 semver1)

Compares two semver objects according to the SemVer precedence rules.
It is a three-way comparison procedure compatible
with [SRFI-67][SRFI-67], returning -1, 0 or 1.

 [SRFI-67]: https://srfi.schemers.org/srfi-67/
 
```Scheme
> (semver-compare (string->semver "1.0.0-alpha") (string->semver "1.0.0"))
-1
```

### (semver-increment-major semver [pre-release-ids build-ids])

Increments the major component of *semver*, returning a new semver
object. Resets minor and patch to zero.

Optionally adds pre-release identifiers and/or build identifiers.

Use this when a project has an incompatible API change.

### (semver-increment-minor semver [pre-release-ids build-ids])

Increments the minor component of *semver*, returning a new semver
object. Resets patch to zero.

Optionally adds pre-release identifiers and/or build identifiers.

Use this when a project adds backwards-compatible new functionality.

### (semver-increment-patch semver [pre-release-ids build-ids])

Increment the patch component of *semver*, returning a new semver
object.

Optionally adds pre-release identifiers and/or build identifiers.

Use this when a project has a backwards-compatible bug fix.

## Ranges API

Semantic Versioning 2.0.0 does not define ranges. Instead, the ranges
API implements the notation from npm's [semver module][ranges].

```Scheme
(import (semver ranges))
```

### (string->semver-range string)

Parse *string* as a semver range.

If *string* does not represent a valid range, **string->semver-range**
raises an exception with condition type **&assertion**.

### (semver-range-desugar range)

Converts all sugary expressions in *range* to simple expressions:
conjunction, disjunction, and simple comparisons with full versions.

### (semver-range->string range)

Returns a string representation of *range*.

### (semver-range->matcher range)

Returns a procedure that matches against *range*. The procedure
returns `#t` if the semver object supplied to it matches the range;
`#f` otherwise.

For convenience *range* may also be a string, in which case it is
first converted to a semver range.

### (semver-in-range? semver range)
    
Returns `#t` if *semver* is in *range*, otherwise returns `#f`.

The arguments may be either a semver object and range object, or
strings.

# LICENSE

Copyright © 2018 Göran Weinholt <goran@weinholt.se>

It is licensed under the MIT license. See [LICENSE](LICENSE).
