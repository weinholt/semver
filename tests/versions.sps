#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

(import
  (rnrs (6))
  (srfi :64 testing)
  (srfi :67 compare-procedures)
  (semver versions))

(test-begin "semver-to-string")
(test-equal "1.0.0"
            (semver->string (make-semver 1 0 0)))
(test-equal "1.0.0-alpha"
            (semver->string (make-semver 1 0 0 '("alpha"))))
(test-equal "1.0.0-alpha+001"
            (semver->string (make-semver 1 0 0 '("alpha") '("001"))))
(test-equal "1.0.0-alpha.1"
            (semver->string (make-semver 1 0 0 '("alpha" 1))))
(test-equal "1.0.0-x.7.z.92"
            (semver->string (make-semver 1 0 0 '("x" 7 "z" 92))))
(test-end)

(test-begin "reject-invalid")
(test-error (make-semver -1 0 0))
(test-error (make-semver 0 1/2 0))
(test-error (make-semver 0 0 +i))
(test-error (make-semver 1 0 0 '("00")))
(test-error (make-semver 1 0 0 '("")))
(test-error (string->semver "1.0.0-"))
(test-error (make-semver 1 0 0 '() '("")))
(test-error (string->semver "1.0.0+"))
(test-end)

(test-begin "string-to-semver")
(test-equal "1.0.0"
            (semver->string (string->semver "1.0.0")))
(test-equal "1.0.0-alpha"
            (semver->string (string->semver "1.0.0-alpha")))
(test-equal "1.0.0-alpha+001"
            (semver->string (string->semver "1.0.0-alpha+001")))
(test-equal "1.0.0+20130313144700"
            (semver->string (string->semver "1.0.0+20130313144700")))
(test-equal "1.0.0-beta+exp.sha.5114f85"
            (semver->string (string->semver "1.0.0-beta+exp.sha.5114f85")))
(test-equal "1.0.0-beta.1e0+1e0"
            (semver->string (string->semver "1.0.0-beta.1e0+1e0")))
(test-equal "1.0.0-beta.0+0"
            (semver->string (string->semver "1.0.0-beta.0+0")))
(test-equal '(0)
            (semver-pre-release-ids (string->semver "1.0.0-0")))
(test-equal '("-")
            (semver-pre-release-ids (string->semver "1.0.0--")))
(test-equal '("-")
            (semver-build-ids (string->semver "1.0.0+-")))
(test-end)

(test-begin "semver-compare")
(test-equal -1 (semver-compare (string->semver "0.0.0") (string->semver "1.0.0")))
(test-equal 0  (semver-compare (string->semver "1.0.0") (string->semver "1.0.0")))
(test-equal 1  (semver-compare (string->semver "1.0.0") (string->semver "0.0.0")))
(test-equal -1 (semver-compare (string->semver "0.0.0") (string->semver "0.1.0")))
(test-equal 0  (semver-compare (string->semver "0.1.0") (string->semver "0.1.0")))
(test-equal 1  (semver-compare (string->semver "0.1.0") (string->semver "0.0.0")))
(test-equal -1 (semver-compare (string->semver "0.0.0") (string->semver "0.0.1")))
(test-equal 0  (semver-compare (string->semver "0.0.1") (string->semver "0.0.1")))
(test-equal 1  (semver-compare (string->semver "0.0.1") (string->semver "0.0.0")))
(test-equal #t (apply chain<? semver-compare (map string->semver
                                                  '("1.0.0"
                                                    "2.0.0"
                                                    "2.1.0"
                                                    "2.1.1"))))
(test-end)

(test-begin "semver-compare-prerel")
(test-equal -1 (semver-compare (string->semver "1.0.0-alpha") (string->semver "1.0.0")))
(test-equal 0  (semver-compare (string->semver "1.0.0-alpha") (string->semver "1.0.0-alpha")))
(test-equal 1  (semver-compare (string->semver "1.0.0") (string->semver "1.0.0-alpha")))
(test-equal #t (apply chain<? semver-compare
                      (map string->semver
                           '("1.0.0-alpha"
                             "1.0.0-alpha.1"
                             "1.0.0-alpha.beta"
                             "1.0.0-beta"
                             "1.0.0-beta.2"
                             "1.0.0-beta.11"
                             "1.0.0-rc.1"
                             "1.0.0"))))
(test-end)

(test-begin "semver-increment")
(test-equal "2.0.0" (semver->string (semver-increment-major (string->semver "1.0.0"))))
(test-equal "1.1.0" (semver->string (semver-increment-minor (string->semver "1.0.0"))))
(test-equal "1.0.1" (semver->string (semver-increment-patch (string->semver "1.0.0"))))
(test-equal "1.0.1-alpha" (semver->string (semver-increment-patch (string->semver "1.0.0")
                                                                  '("alpha"))))
(test-equal "1.0.1-alpha+001" (semver->string (semver-increment-patch (string->semver "1.0.0")
                                                                      '("alpha")
                                                                      '("001"))))
(test-end)


(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
